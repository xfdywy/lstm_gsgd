import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.optim import Optimizer
import torch.nn.parallel
import numpy as np
import scipy.sparse as sparse
import torchvision.models

import random
import math

class SGD(Optimizer):
    def __init__(self, params, lr=0.1, momentum=0.0, dampening=0,
                 weight_decay=0, nesterov=False):
        defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
                        weight_decay=weight_decay, nesterov=nesterov)
        if nesterov and (momentum <= 0 or dampening != 0):
            raise ValueError("Nesterov momentum requires a momentum and zero dampening")
        super(SGD, self).__init__(params, defaults)
        self.ec_layer = None

    def cal_ec_layer(self, ec_layer):
        ec_layer = list(ec_layer.values())
        res =[]
        self.unpacklist(ec_layer, res)
        self.ec_layer = res




    def unpacklist(self, a, b):
        if not isinstance(a, list):
            b.append(a)
        else:
            for ii in a:
                self.unpacklist(ii, b)

    def __setstate__(self, state):
        super(SGD, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('nesterov', False)

    def pre_step(self):
        pass
    #
    def step(self, closure=None):
        # assert 1==2
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            lr = group['lr']
            weight_decay = group['weight_decay']
            momentum = group['momentum']
            dampening = group['dampening']
            # dampening = momentum
            nesterov = group['nesterov']

            for p in group['params']:
                if p.grad is None:
                    continue
                d_p = p.grad.data * lr
                if weight_decay != 0:
                    d_p.add_(weight_decay * lr, p.data)
                if momentum != 0:
                    param_state = self.state[p]
                    if 'momentum_buffer' not in param_state:
                        buf = param_state['momentum_buffer'] = torch.zeros(p.data.size()).cuda()
                        buf.mul_(momentum).add_(d_p)
                    else:
                        buf = param_state['momentum_buffer']
                        buf.mul_(momentum).add_((1 - dampening), d_p)
                    if nesterov:
                        d_p = d_p.add(momentum, buf)
                    else:
                        d_p = buf

                p.data.add_(-1, d_p)
        return loss




    def partial_bp_step(self, closure=None):
        if self.ec_layer is None:
            raise Exception('please call cal_ec_layer first')
        # print(self.ec_layer, '!!!!')
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            lr = group['lr']
            weight_decay = group['weight_decay']
            momentum = group['momentum']
            dampening = group['dampening']
            # dampening = momentum
            nesterov = group['nesterov']

            for layer_idx, p in enumerate(group['params']):
                isinconvidx = layer_idx in self.ec_layer

                if isinconvidx:

                    continue
                #


                if p.grad is None:
                    continue
                d_p = p.grad.data * lr

                # print(layer_idx,weight_decay, momentum,lr )

                if weight_decay != 0:
                    d_p.add_(weight_decay * lr, p.data)
                if momentum != 0:
                    param_state = self.state[p]
                    if 'momentum_buffer' not in param_state:
                        buf = param_state['momentum_buffer'] = torch.zeros(p.data.size()).cuda()
                        buf.mul_(momentum).add_(d_p)
                    else:
                        buf = param_state['momentum_buffer']
                        buf.mul_(momentum).add_((1 - dampening), d_p)
                    if nesterov:
                        d_p = d_p.add(momentum, buf)
                    else:
                        d_p = buf

                p.data.add_(-1, d_p)
        return loss



