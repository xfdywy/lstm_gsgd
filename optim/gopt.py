import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.optim import Optimizer
import torch.nn.parallel
import numpy as np
import scipy.sparse as sparse
import torchvision.models
from .SGD import SGD
# from opt.Adam import Adam
# from opt.RMSprop import RMSprop
from .gopt_method import gSGD_method

import random
import math


class gSGD(gSGD_method):
    ## need args   ec_layer_type device opt

    def __init__(self, models, extra_params, lr,  lr_inner,  args, momentum=0, dampening=0,
                 weight_decay=0, nesterov=False):
        gSGD_method.__init__(self,args=args)

        self.models = models
        self.extra_params = extra_params
        self.param_groups=[ {'lr' : lr,
                             'lr0' : lr,
                             'lr_inner' : lr_inner,
                             'lr0_inner' : lr_inner}]
        self.params = list(models.parameters())
        self.args = args
        self.ec_layer = self.cal_ec_layer()
        self.mask = self.cal_mask(models)
        self.cal_internal_optim()
        self.internal_optim.cal_ec_layer(self.ec_layer)



    def cal_ec_layer(self):
        if self.args.ec_layer_type == 0:
            ec_layer = self.models.ec_layer
        elif self.args.ec_layer_type == 1:
            ec_layer = self.cal_ec_layer_type1()
        elif self.args.ec_layer_type ==2 :
            ec_layer = self.cal_ec_layer_type2()
        return ec_layer

    def cal_ec_layer_type1(self):
        weight_layer = []
        is_hh = []
        hidden_size = [self.args.nhid]

        for nii, ii in enumerate(self.models.named_parameters()):
            if 'rnn' in ii[0]:
                if 'weight' in ii[0]:
                    weight_layer.append(nii)
                    if '_hh_' in ii[0]:
                        is_hh.append(True)
                        # self. = ii[1].shape
                    else:
                        is_hh.append(False)
                        if '_ih_' in ii[0]:
                            hidden_size.append(ii[1].shape[0] / 4)
        self.is_hh = is_hh
        # {
        #     'lstm': [is_hh]
        # }

        ec_layer = {
            'lstm': [weight_layer]
        }


        return (ec_layer )

    def cal_internal_optim(self):
        if self.args.inner_opt == 'sgd':
            self.internal_optim = SGD(list(self.models.parameters()) + self.extra_params, lr=self.param_groups[0]['lr_inner'])
        else:
            assert 1==2

    def cal_mask(self, model):
        self.num_layer = len(self.params)
        mask = {'cnn':[],
                'mlp':[],
                'lstm':[],}
        s_h = {'cnn':[],
                'mlp':[],
                'lstm':[],}
        s_idx = {'cnn':[],
                'mlp':[],
                'lstm':[],}

        for blocks_type, blocks_idx in self.ec_layer.items():
            if blocks_type =='cnn':
                print('not implement!')
                assert 1==2
            elif blocks_type =='mlp' :
                print('not implement !')
                assert 1==2
            elif blocks_type == 'lstm':
                if len(blocks_idx) ==0 :
                    continue
                self.s_idx = 0
                # num_layer = len(blocks_idx)
                # num_hidden = num_layer - 1
                mask_lstm  = []
                s_idx_lstm = []
                s_h_lstm = []

                # tmp = [0, 0]
                for block in blocks_idx:
                    layer_mask = []
                    num_layer_in_block = len(block)
                    tmp = [0,0]

                    for ii in range(num_layer_in_block - 1):
                        if not self.is_hh[ii]:
                            # this_layer_idx =
                            h = int(self.params[block[ii]].shape[0]/4)
                            if h > tmp[1]:
                                tmp = [ii, h]

                layer_s_idx = tmp[0]  #   sub layer index, output size max
                layer_s_h = tmp[1]  #  size max output size

                for ec_layer_idx, layer_idx in enumerate(block):
                    layer = self.params[layer_idx]
                    outcome,income = layer.shape
                    if not self.is_hh[ec_layer_idx]:
                        this_is_hh = False
                        if ec_layer_idx == 0:
                            loc = 'f'
                        elif ec_layer_idx == num_layer_in_block - 1:
                            loc = 'l'
                        else:
                            loc = 'm'

                        mask_tmp = self.generate_eye_lstm(outcome, income, loc, this_is_hh).to(self.args.device)

                        this_mask_red = (mask_tmp == 1).to(torch.float32)
                        # if loc == 'f':
                        #     layer.data = layer.data * (1 - this_mask_red) + this_mask_red * 0.5
                        # else:
                        layer.data = layer.data * (1 - this_mask_red) + this_mask_red



                    else:
                        this_is_hh = True
                        mask_tmp = self.generate_eye_lstm(outcome, income, loc, this_is_hh).to(self.args.device)
                        this_mask_red = (mask_tmp == 1).to(torch.float32)
                        layer.data = layer.data * (1 - this_mask_red) + this_mask_red

                    layer_mask.append(mask_tmp)

                mask_lstm.append(layer_mask)
                s_h_lstm.append(layer_s_h)
                s_idx_lstm.append(layer_s_idx)

            mask['lstm'] = mask_lstm
            s_h['lstm'] = s_h_lstm
            s_idx['lstm'] = s_idx_lstm

        self.s_h = s_h
        self.s_idx = s_idx
        return (mask)




    def step(self, closure=None):
        if self.args.ec_opt == "ec":
            # self.pre_step()
            self.ec_step(closure)
            self.bp_partial_step(closure)
        elif self.args.ec_opt == 'bp':
            self.bp_step(closure)

    def pre_step(self):
        self.internal_optim.pre_step()

    def bp_partial_step(self, closure):
        self.internal_optim.partial_bp_step()

    def bp_step(self, closure):
        self.internal_optim.step()

    def ec_step(self, closure=None):
        """Performs a single optimization step."""
        # lr = self.lr_t
        # print(self.lr_t,self.lr_0)

        for blocks_type, blocks_idx in self.ec_layer.items():
            if blocks_type =='cnn':
                if len(blocks_idx) ==0:
                    continue
                self.ec_step_cnn(blocks_idx)
            elif blocks_type =='mlp':
                if len(blocks_idx) ==0:
                    continue
                self.ec_step_mlp(blocks_idx)
            elif blocks_type =='lstm':
                if len(blocks_idx) ==0:
                    continue
                self.ec_step_lstm(blocks_idx)
            else:
                assert 1==2

