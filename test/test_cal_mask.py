import torch
import os
os.chdir('../')
from optim import gopt

args, model = torch.load('test.pt')

args.ec_layer_type =1
args.device='cpu'
args.opt = 'gsgd'
args.lr = 1

args.ec_opt = 'ec'

args.inner_opt = 'sgd'
args.decay = 'none'

opt = gopt.gSGD(model,[],1,1,args)


mask = opt.mask['lstm'][0]
ii, param_name, param_shape, params = list(
        zip(*[[nii,ii[0],ii[1].shape,ii[1]]
        for nii,ii in enumerate(model.named_parameters())]))

for ii in model.parameters():
    ii.grad = ii.data
    
    
opt.step()
print(torch.nonzero(opt.mask['lstm'][0][1]==0))
